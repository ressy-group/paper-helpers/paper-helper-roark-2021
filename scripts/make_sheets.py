#!/usr/bin/env python

import re
import sys
from pathlib import Path
from csv import DictWriter
from Bio import SeqIO

def make_sheets(dir_genbank, out_v, out_mabs):
    rows_v = []
    rows_mabs = {}
    for fasta in Path(dir_genbank).glob("*.fasta"):
        for rec in SeqIO.parse(fasta, "fasta"):
            match = re.match(r"^([^ ]+) .* (IG[HKL]V[0-9]+-.*_S[0-9]+) .*", rec.description)
            if match:
                acc, gene = match.groups()
                rows_v.append({
                    "Accession": acc,
                    "Segment": gene[:4],
                    "ID": gene,
                    "Seq": str(rec.seq)})
            else:
                match = re.match(r"([^ ]+) Macaca mulatta (clone|isolate) ([^ ]+) .* (light|heavy) .*", rec.description)
                acc, _, seqid, chain = match.groups()
                mab = re.sub("V[HKL]$", "", seqid)
                lineage = re.sub(r"(RHA1\.V2).*", r"\1", re.sub(r"(DH[0-9]+).*", r"\1", mab))
                if mab not in rows_mabs:
                    rows_mabs[mab] = {}
                row = rows_mabs[mab]
                prefix = chain.capitalize()
                if "UCA" in mab:
                    cat = "UCA"
                elif re.match(".*IA[0-9]+", mab):
                    cat = "anc"
                else:
                    cat = "mAb"
                row["Lineage"] = lineage
                row["Antibody"] = mab
                row["Category"] = cat
                row[f"{prefix}Accession"] = acc
                row[f"{prefix}ID"] = seqid
                row[f"{prefix}Seq"] = str(rec.seq)

    rows_v.sort(key=lambda row: row["Accession"])
    with open(out_v, "w") as f_out:
        writer = DictWriter(f_out, ["Accession", "Segment", "ID", "Seq"], lineterminator="\n")
        writer.writeheader()
        writer.writerows(rows_v)

    rows_mabs = list(rows_mabs.values())
    rows_mabs.sort(key=lambda row: (row["Lineage"], row["Antibody"]))
    with open(out_mabs, "w") as f_out:
        writer = DictWriter(
            f_out,
            ["Lineage", "Antibody", "Category", "HeavyAccession", "HeavyID", "HeavySeq", "LightAccession", "LightID", "LightSeq"],
            lineterminator="\n")
        writer.writeheader()
        writer.writerows(rows_mabs)


if __name__ == "__main__":
    make_sheets(sys.argv[1], sys.argv[2], sys.argv[3])
