with open("from-paper/genbank_accessions_abs.txt") as f_in:
    GB_ACCESSIONS_ABS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS_ABS)

rule output:
    output:
        v="output/v.csv",
        mabs="output/mabs.csv"
    input: TARGET_GBF_FASTA
    shell: "python scripts/make_sheets.py from-genbank {output.v} {output.mabs}"

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule download_gbf_fa:
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} {output}"
