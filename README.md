# Antibody GenBank entries from Roark 2021

Gathering and parsing antibody GenBank entries from:

Recapitulation of HIV-1 Env-antibody coevolution in macaques leading to neutralization breadth.
Ryan S. Roark et al. 
Science 371, eabd2638 (2021)
DOI:[10.1126/science.abd2638](https://doi.org/10.1126/science.abd2638)

(Yes, our own paper.)
